# Live Attention Detection for Online Classes

The project aims at monitoring attention of candidates during a online lecture, through facial recognition, eye movement, system audio levels etc.

Run Instructions
For candidates:
1. Start the application just before online lesson.
2. Login into system with provided credntials and session code. (At first login candidates are prompted to change the password)
3. Close the application after completion of lessons


For admins
Start the application
1. Application will need 10 minutes to load data from cloud wait for a while.
2. Add new candidates to data by user id and password.
3. Or add them by uploading csv file of same
4. Adding already existing candidate will give error
5. View Visualisations
6. Get Reports


